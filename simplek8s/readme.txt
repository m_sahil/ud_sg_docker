
brew install kubectl
which kubectl
brew cask install minikube
which minikube
minikube start
minikube ip (To get ip)

kubectl apply -f <path to config file>
kubectl apply -f client-pod.yaml
kubectl apply -f client-node-port.yaml

Getting status:
kubectl get pods
kubectl get services

Get details on configuration of an object
kubectl describe <object type>
kubectl describe <object type> <object name>
kubectl describe pod client-pod
kubectl describe pods

Removing existing object
kubectl delete -f <config file used to create object>
kubectl delete -f client-pod.yaml

Deployment
kubectl apply -f client-deployment.yaml
kubectl get deployments

kubectl get pods -o wide (Every single pod gets its own IP internal to VM)

Imperative command to use specific image version
docker build -t sahil1/multi-client:v5 .
docker push sahil1/multi-client:v5
kubectl set image <object type>/<object name> <container name>=<new image to use>

kubectl set image deployment/client-deployment client=sahil1/multi-client:v5


Configure the VM to use your docker server.
Below command only configures current terminal window:
eval $(minikube docker-env)
