docker build -t sahil1/multi-client:latest -t sahil1/multi-client:$SHA -f ./client/Dockerfile ./client
docker build -t sahil1/multi-server:latest -t sahil1/multi-server:$SHA -f ./server/Dockerfile ./server
docker build -t sahil1/multi-worker:latest -t sahil1/multi-worker:$SHA -f ./worker/Dockerfile ./worker

docker push sahil1/multi-client:latest
docker push sahil1/multi-server:latest
docker push sahil1/multi-worker:latest

docker push sahil1/multi-client:$SHA
docker push sahil1/multi-server:$SHA
docker push sahil1/multi-worker:$SHA

kubectl apply -f k8s
kubectl set image deployments/server-deployment server=sahil1/multi-server:$SHA
kubectl set image deployments/client-deployment client=sahil1/multi-client:$SHA
kubectl set image deployments/worker-deployment worker=sahil1/multi-worker:$SHA
